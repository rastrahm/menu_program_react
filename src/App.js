import React from 'react';
import { Fragment } from 'react';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink
} from 'reactstrap';
import {
	BrowserRouter as Router,
	Switch,
	Route,
	Link,
	Redirect
} from "react-router-dom";
import AppPage from './AppPage';
import AppLink from './AppLink';
import AppForm from './AppForm';

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
			isOpen: false,
			options: []
		};
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
	}
	
	//Carga los elementos basicos del menú
	componentDidMount() {
		fetch("http://phpreactproxy.local/outFunctions/menu")
      	.then(res => res.json())
      	.then(
        	(result) => {
          		this.setState({
            		options: result.data
				});
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
			this.setState({
				isLoaded: true,
				error
			});
        })
	}

	//Llama a AppPage para llamar y renderizar este componente
	SendPage = (link) => {
		return <AppPage route={link}></AppPage>
	}

    render() {
		return <Fragment >
			<Router>
				<Navbar color = "dark" dark expand = "md">
					<NavbarBrand href="/">Menu Program</NavbarBrand>
					<NavbarToggler onClick={this.toggle} />
					<Collapse navbar>
						<Nav navbar>
							{this.state.options.map(link => {
								return (
									<NavItem key={link.big_id.toString()}>
										<NavLink> 
											<Link to={"/" + link.str_link}>{link.str_link} </Link>
										</NavLink>
									</NavItem>
								);
							})}
						</Nav>
					</Collapse>
				</Navbar>
				<Switch>
					{this.state.options.map(link => {
						return (
							<Route path={"/" + link.str_link} component={() => <AppLink link={link.str_link} />}>
							</Route>
						);
					})}
					<Route exact path="/" render={() => (<Redirect to="/Inicio"/>)}/>
				</Switch>
			</Router>
			<AppForm></AppForm>
		</Fragment>
    }
}