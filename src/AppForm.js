import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

export default class AppForm extends React.Component {
    constructor() {
    	super();
    	this.state = {
			formControls: []
		}
	}

	formSubmitHandler = () => {
		const formData = {};
	}
  

	render() {
		return (
			<Form>
				<FormGroup>
					<Label for="user">Usuario</Label>
					<Input type="text" name="user" id="user" placeholder="Introduzca su usuario" />
				</FormGroup>
				<FormGroup>
					<Label for="pass">Password</Label>
					<Input type="password" name="pass" id="pass" placeholder="Introduzca su password" />
				</FormGroup>
				<Button>Submit</Button>
			</Form>
		);
	}
}