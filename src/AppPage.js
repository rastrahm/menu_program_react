import React from 'react';

export default class AppPage extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
			isOpen: false,
			isLink: "",
			data: []
		}
    }

    toggle() {
        this.setState({
			isOpen: !this.state.isOpen,
			isLink: "",
			data: []
		});
    }

    //Carga los elementos basicos del menú
    componentDidMount() {
		if (this.props.link !== undefined) {
			fetch("http://phpreactproxy.local/outFunctions/" + this.props.link)
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						isLink: this.props.link,
						data: result.data[0]
					});
				},
				// Note: it's important to handle errors here
				// instead of a catch() block so that we don't swallow
				// exceptions from actual bugs in components.
				(error) => {
					this.setState({
						isLoaded: true,
						error
					});
			})
		} else {
			this.setState({data: [{txt_article: ""}]});
		}
	}
	
	returnPage (){
		if (this.props.link !== this.props.isLink) {
			fetch("http://phpreactproxy.local/outFunctions/" + this.props.route.link)
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({
						isLink: this.props.link,
						data: result.data
					});
					this.render();
				},
				// Note: it's important to handle errors here
				// instead of a catch() block so that we don't swallow
				// exceptions from actual bugs in components.
				(error) => {
					this.setState({
						isLoaded: true,
						error
					});
			})
		}
	}

    render() {
        return <div dangerouslySetInnerHTML={{__html: this.state.data.txt_article}}></div>
    }
}